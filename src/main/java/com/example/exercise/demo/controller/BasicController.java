package com.example.exercise.demo.controller;

import com.example.exercise.demo.model.JsonModel;
import com.example.exercise.demo.service.ModelAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicController {

    @Autowired
    ModelAccountService modelAccountService;

    @PostMapping(
            value =  "/json"
    )
    public ResponseEntity<JsonModel> getValue(
            @RequestBody JsonModel jsonModel
    ) {
        return new ResponseEntity<>(modelAccountService.getJson(jsonModel),
                HttpStatus.OK);
    }

    @PostMapping(
            value = "/jsonToXml"
    )
    public ResponseEntity<String> jsonToXml(
            @RequestBody JsonModel jsonModel
    ) {
        return new ResponseEntity<>(modelAccountService.returnXml(jsonModel),
                HttpStatus.OK);
    }

    //Create another controller same above but different interface

}
