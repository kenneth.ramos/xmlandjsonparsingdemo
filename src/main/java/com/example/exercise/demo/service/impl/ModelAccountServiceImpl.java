package com.example.exercise.demo.service.impl;

import com.example.exercise.demo.model.ConstantsData;
import com.example.exercise.demo.model.JsonModel;
import com.example.exercise.demo.service.ModelAccountService;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

@Service
public class ModelAccountServiceImpl implements ModelAccountService {

    @Override
    public JsonModel getJson(JsonModel request) {
        request.setName(request.getName());
        request.setJobTitle(request.getProjectTitle());

        return new JsonModel(request.getName(), request.getProjectTitle());
    }

    @Override
    public String returnXml(JsonModel request) {
        request.setName(request.getName());
        request.setJobTitle(request.getProjectTitle());

        String toJson = "{root: [{ " + ConstantsData.NAME.toLabel() + ": " + request.getName() + ", "
                + ConstantsData.PROJECTTITLE.toLabel() + ": " + request.getProjectTitle() + "}]}";
        JSONObject json = new JSONObject(toJson);
        String xml = XML.toString(json);

        return xml;
    }

    //@Override the interface
    //Create returnToJson(JsonModel) that will return String when called on the controller
    //based on my implementation I only have 4 line of codes + 1 for the return
}