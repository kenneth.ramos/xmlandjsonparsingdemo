package com.example.exercise.demo.service;

import com.example.exercise.demo.model.JsonModel;

public interface ModelAccountService {
    JsonModel getJson (JsonModel request);
    String returnXml (JsonModel request);
    //Add same interface then implement to impl
}
