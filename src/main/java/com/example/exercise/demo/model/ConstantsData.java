package com.example.exercise.demo.model;

public enum ConstantsData {

    NAME("Name"),
    PROJECTTITLE("ProjectTitle");

    private String label;

    ConstantsData(String label) {
        this.label = label;
    }

    public String toLabel() {
        return label;
    }

}
