package com.example.exercise.demo.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class JsonModel {

    private String name;
    private String projectTitle;

    public JsonModel() {

    }

    public JsonModel(String name, String projectTitle) {
        this.name = name;
        this.projectTitle = projectTitle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setJobTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

}
